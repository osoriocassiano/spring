package com.amigoscode.learnspring.core.service;

import java.util.regex.Pattern;

public class EmailExtractor extends ExtractorReport {

	@Override
	public Pattern getPattern() {
		return Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	}

	@Override
	public String getReportName() {
		return "Email";
	}

	@Override
	public String clean(String input) {
		return input.toUpperCase();
	}

}
