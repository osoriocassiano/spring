package com.amigoscode.learnspring.core.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

abstract public class ExtractorReport {

	public abstract Pattern getPattern();

	public abstract String getReportName();

	public abstract String clean(String input);

	private String parse(String path) throws FileNotFoundException {

		StringBuilder out = new StringBuilder();
		File file = new File(path);
		Scanner scanner = new Scanner(file);
		if (scanner.hasNext()) {
			scanner.nextLine();
		} else {
			return "Empty file";
		}

		while (scanner.hasNext()) {
			String nextLine = scanner.nextLine();
			Matcher matcher = getPattern().matcher(nextLine);

			boolean matches = matcher.matches();

			if (matches) {
				out.append(this.clean(nextLine) + "\n");
//				out += this.clean(nextLine) + "\n";
			}
		}
		return out.isEmpty() ? "Empty file" : out.toString();
	}

	public void prepareAndSendReport(String path) throws FileNotFoundException {
		System.out.println("Starting report " + this.getReportName() + " ...");
		String report = this.parse(path);
		System.out.println(report);
		System.out.println("Sent report " + this.getReportName());
	}

}
