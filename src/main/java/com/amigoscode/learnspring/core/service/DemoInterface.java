package com.amigoscode.learnspring.core.service;

interface X {
	void show();

	void config();
}

interface Y {
	void run();
}

class A implements X, Y {

	@Override
	public void show() {
		System.out.println("In show");

	}

	@Override
	public void config() {
		System.out.println("In config");

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

}
