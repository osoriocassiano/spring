package com.amigoscode.learnspring.core.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amigoscode.learnspring.core.entity.Student;
import com.amigoscode.learnspring.core.repository.StudentRepository;

@Service
public class StudentServiceImpl {

	private final StudentRepository studentRepository;

	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository) {
		super();
		this.studentRepository = studentRepository;
	}

	public List<Student> getStudent() {
		return this.studentRepository.findAll();
	}

	public void addNewStudent(Student student) {
		Optional<Student> studentOpt = studentRepository.findStudentByEmail(student.getEmail());
		if (studentOpt.isPresent()) {
			throw new IllegalStateException("Email taken");
		}
		studentRepository.save(student);

	}

	public void deleteStudent(Long studentId) {
		boolean exists = this.studentRepository.existsById(studentId);
		if (!exists) {
			throw new IllegalAccessError("student with id" + studentId + " does not exists");
		}

		this.studentRepository.deleteById(studentId);
	}

}
