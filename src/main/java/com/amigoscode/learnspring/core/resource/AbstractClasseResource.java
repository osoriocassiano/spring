package com.amigoscode.learnspring.core.resource;

import java.io.FileNotFoundException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amigoscode.learnspring.core.service.EmailExtractor;

@RestController
@RequestMapping("abstract-class")
public class AbstractClasseResource {

	@GetMapping()
	public void abstractClass() throws FileNotFoundException {
		String filePath = this.getClass().getClassLoader().getResource("reports/data.txt").getFile();
//		new NumberExtractorReport().prepareAndSendReport(filePath);
		new EmailExtractor().prepareAndSendReport(filePath);

	}

}
