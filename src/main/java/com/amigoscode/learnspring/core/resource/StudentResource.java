package com.amigoscode.learnspring.core.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amigoscode.learnspring.core.entity.Student;
import com.amigoscode.learnspring.core.service.StudentServiceImpl;

@RestController
@RequestMapping(path = "api/v1/student")
public class StudentResource {

	private final StudentServiceImpl studentService;

	@Autowired
	public StudentResource(StudentServiceImpl studentService) {
		this.studentService = studentService;
	}

	@GetMapping
	public List<Student> getStudent() {
		return this.studentService.getStudent();
	}

	@PostMapping
	public Student registerNewStudent(@RequestBody Student student) {
		this.studentService.addNewStudent(student);
		return null;
	}

	@DeleteMapping("{studentId}")
	public void deleteStudent(@PathVariable("studentId") Long studentId) {
		this.studentService.deleteStudent(studentId);
	}

}
