package com.amigoscode.learnspring.config;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import org.springframework.boot.CommandLineRunner;

import com.amigoscode.learnspring.core.entity.Student;
import com.amigoscode.learnspring.core.repository.StudentRepository;

//@Configuration
public class StudentConfig {

	// @Bean
	CommandLineRunner commandLineRunner(StudentRepository repository) {
		return args -> {
			Student mariam = new Student(1L, "Mariam", "mariam@gmail.com", LocalDate.of(2000, Month.JANUARY, 5));
			Student james = new Student(1L, "James", "james@gmail.com", LocalDate.of(2001, Month.JANUARY, 4));
			repository.saveAll(List.of(mariam, james));
		};
	}

}
