package com.amigoscode.learnspring.telusko;

enum Status {
	Running, Failed, Pending, Success
}

enum Laptop {
	MAckbook(2000), XPS(2200), Surface, Thinkpad(1500);

	private int price;

	Laptop(int price) {
		this.price = price;
	}

	Laptop() {
		price = 500;
		System.out.println("In constructor with args");
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}

public class DemoEnum {

	public static void main(String args[]) {

		Status s = Status.Success;

		switch (s) {
		case Running:
			System.out.println("All good");
			break;
		case Failed:
			System.out.println("Try again");
			break;
		case Pending:
			System.out.println("Please wait");
			break;
		case Success:
			System.out.println("Done");
			break;
		default:
			System.out.println("Deafult");
			break;
		}

		System.out.println("END OF SWITCH");

		for (Laptop lap : Laptop.values()) {
			System.out.println(lap + " : " + lap.getPrice());
		}

	}

}
