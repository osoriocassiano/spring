package com.amigoscode.learnspring.telusko;

interface X {
	void show();

	void config();
}

interface Y {
	void run();
}

class A implements X, Y {

	@Override
	public void show() {
		System.out.println("In Show");
	}

	@Override
	public void config() {
		System.out.println("In Config");
	}

	@Override
	public void run() {
		System.out.println("In run");
	}

}

public class DemoInterface {

	public static void main(String args[]) {
		X objA = new A();
		objA.show();
		objA.config();

		Y obj2 = new A();
		obj2.run();
	}

}
