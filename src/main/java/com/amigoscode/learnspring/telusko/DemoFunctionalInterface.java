package com.amigoscode.learnspring.telusko;

@FunctionalInterface
interface AA {
	void show(int i);
}

@FunctionalInterface
interface Add {
	int add(int i, int j);
}

//interface AA {
//	void show();
//}

//class B implements AA {
//
//	@Override
//	public void show() {
//		System.out.println("In show");
//	}
//
//}

public class DemoFunctionalInterface {
	public static void main(String args[]) {
//		AA obj = new B();
//		obj.show();

//		AA obj = new AA() {
//			public void show() {
//				System.out.println("In show");
//			}
//		};
//		obj.show();

//		AA obj = (i) -> System.out.println("In show " + i);
//		obj.show(5);
//		System.out.println("===========");

		Add sumObj = (i, j) -> i + j;

		int sumResult = sumObj.add(5, 4);
		System.out.println(sumResult);
	}
}
